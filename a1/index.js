/*
	Create a trainer object using object literals.
	Initialize/add the following trainer object properties:
		- Name (String)
		- Age (Number)
		- Pokemon (Array)
		- Friends (Object with Array values for properties)

	Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
*/
const trainer = {
	name: 'Rene Tajos Jr.',
	age: 25,
	pokemon: ['Pikachu', 'Ivysaur', 'Venusaur', 'Charmander'],
	friends: {
		jojie: ['Bulbasaur', 'Squirtle'],
		eziel: ['Wartotle', 'Caterpie']
	},
	talk: function() {
		log("Pikachu! I choose you!");
	}
}

log(trainer)
// Access the trainer object properties using dot
log('Result of dot notation:')
log(trainer.name)
// Access the trainer object properties using square bracket
log('Result of square bracket notation:')
log(trainer['pokemon'])
// Invoke/call the trainer talk object method.
log('Result of talk method:')
trainer.talk()

/*
Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
*/

function Pokemon(name, level) {
	this.name = name
	this.level = level
	this.health = level * 10
	this.attack = level * 5
	this.isFainted = false

	/**
	 * Create a tackle method that will subtract the health property of the 
	 * target pokemon object with the attack property of the object that used 
	 * the tackle method.
	 * 
	 * */
	this.tackle = function(target) {
		log('')
		target.health -= this.attack

		log(`${this.name} tackled ${target.name}!`)
		log(`${target.name} health is now reduced to ${target.health}!`)

		// Create a condition in the tackle method that if the 
		// health property of the target pokemon object is less 
		// than or equal to 0 will invoke the faint method.
		if (target.health <= 0) {
			target.faint()
		}

		log(target)
		if (target.isFainted) {
			log(`\n${this.name} WON THE MATCH.!!!`)
		}
	}

	/**
	 * Create a faint method that will print out a message 
	 * of targetPokemon has fainted.
	 * */
	this.faint = function() {
		this.isFainted = true
		log(`${this.name} has fainted.`)
	}
}

// Create/instantiate several pokemon object from the constructor with 
// varying name and level properties.
log('')
log('Contestants:')

const pikachu = new Pokemon(trainer.pokemon[0], 5)
log(pikachu)

const squirtle = new Pokemon(trainer.friends.jojie[1], 4)
log(squirtle)

log('')
log('Combat arena:')

// Invoke the tackle method of one pokemon object to see if it works as intended.
pikachu.tackle(squirtle)
squirtle.tackle(pikachu)
pikachu.tackle(squirtle)

// utility
function log(obj) {
	console.log(obj)
}